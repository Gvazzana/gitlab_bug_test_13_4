# Gitlab_bug_test_13_4

This project tests a bug in Gitlab 13.4, where a child pipeline calling a child pipeline does not run bridge jobs.